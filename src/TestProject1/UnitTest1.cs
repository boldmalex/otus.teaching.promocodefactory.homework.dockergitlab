using Microsoft.VisualStudio.TestTools.UnitTesting;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System.Linq;

namespace TestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var test = FakeDataFactory.Customers.FirstOrDefault();
            Assert.IsFalse(test == null);
            Assert.IsTrue(test.FirstName.ToUpper() == "ИВАН");
        }
    }
}
